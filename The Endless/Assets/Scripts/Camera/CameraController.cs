﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraController : MonoBehaviour, IOnTouchPanEvent, IOnTouchPinchEvent
{
    [SerializeField]
    float panspeed = 0.1f;
    [SerializeField]
    float zoomspeed = 10;

    private void Awake()
    {

    }

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void OnTouchPan(Vector2 position, Vector2 deltaposition)
    {
        GetComponent<Camera>().transform.position += new Vector3(-deltaposition.x, 0, -deltaposition.y) * panspeed;
    }

    public void OnTouchPinch(Vector2 position1, Vector2 position2, Vector2 deltaposition1, Vector2 deltaposition2, float distance, float deltadistance)
    {
        GetComponent<Camera>().fieldOfView -= deltadistance * zoomspeed;
        GetComponent<Camera>().fieldOfView = Mathf.Clamp(GetComponent<Camera>().fieldOfView, 10, 120);
    }
}
