﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UIController : MonoBehaviour {

    public static UIController Instance { get; set; }

    private List<GameObject> MainUI = new List<GameObject>(); //Primary UI that always are visible on a certain scene
    private List<GameObject> OneUI = new List<GameObject>(); // List only one active UI - for big ass UI elements.

    void Awake()
    {
        Instance = this;
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void AddMainUIElement(GameObject ui, bool active = true)
    {
        MainUI.Add(ui);
        ui.SetActive(active);
    }

    public void RemoveMainUIElement(GameObject ui)
    {
        MainUI.Remove(ui);
        ui.SetActive(false);
    }

    public void HideMainUI()
    {
        foreach (var item in MainUI)
        {
            item.SetActive(false);
        }
    }

    public void ShowMainUI()
    {
        foreach (var item in MainUI)
        {
            item.SetActive(true);
        }
    }


    //===============================
    // One item UI
    //===============================

    public void Show(GameObject ui)
    {
        if (CanShow())
        {
            OneUI.Add(ui);
            ui.SetActive(true);
        }
    }

    public void Close (GameObject ui)
    {
        if (OneUI.Contains(ui))
        {
            ui.SetActive(false);
            OneUI.Clear(); //better safe than sorry
        }

    }

    public bool CanShow()
    {
        return OneUI.Capacity < 1;
    }
}
