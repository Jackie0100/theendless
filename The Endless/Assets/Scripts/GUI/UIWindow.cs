﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UIWindow : MonoBehaviour {


    [SerializeField]
    UnityEvent _onWindowOpened;
    [SerializeField]
    UnityEvent _onWindowClosed;
    static UIWindow _currentFocusedWindow;

    /// <summary>
    /// Gets the window which is currently in focus
    /// </summary>
    public static UIWindow currentFocusedWindow
    {
        get
        {
            return _currentFocusedWindow;
        }
        protected set
        {
            if (_currentFocusedWindow != null)
            {
                _currentFocusedWindow.CloseWindow();
            }
            _currentFocusedWindow = value;
        }
    }

    void Awake()
    {
        currentFocusedWindow = this;
    }

    /// <summary>
    /// Event for when the window closes/disabled.
    /// </summary>
    public UnityEvent onWindowClosed
    {
        get
        {
            return _onWindowClosed;
        }
        set
        {
            _onWindowClosed = value;
        }
    }

    /// <summary>
    /// Event for when the window opens/enabled.
    /// </summary>
    public UnityEvent onWindowOpened
    {
        get
        {
            return _onWindowOpened;
        }
        set
        {
            _onWindowOpened = value;
        }
    }

    /// <summary>
    /// Invokes event when enabled/opened
    /// </summary>
    protected void OnEnable()
    {
        _onWindowOpened.Invoke();
    }

    /// <summary>
    /// Invokes event when disabled/closes
    /// </summary>
    protected void OnDisable()
    {
        _onWindowClosed.Invoke();
    }

    /// <summary>
    /// Opens the window by setting it to active.
    /// </summary>
    public virtual void OpenWindow()
    {
        OpenWindow(true);
    }

    /// <summary>
    /// Opens the window by setting it to active and set it to either being in focus or unfocused.
    /// </summary>
    /// <param name="focusOnOpen">Decide if the window should be in focus when opened.</param>
    public virtual void OpenWindow(bool focusOnOpen)
    {
        if (!this.gameObject.activeSelf)
        {
            this.gameObject.SetActive(true);
            if (focusOnOpen)
            {
                currentFocusedWindow = this;
                this.GetComponent<RectTransform>().SetAsLastSibling();
            }
        }
    }

    /// <summary>
    /// Closes the window and removes its focus
    /// </summary>
    public virtual void CloseWindow()
    {
        if (this.gameObject.activeSelf)
        {
            if (_currentFocusedWindow == this)
            {
                _currentFocusedWindow = null;
            }
            this.gameObject.SetActive(false);
        }
    }
}
