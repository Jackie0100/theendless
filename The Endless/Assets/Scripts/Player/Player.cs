﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
    int maxLevel = 100;

    [SerializeField]
    private float _xp;

    public int level { get { return CalculateLevel(1, _xp); } }
    public float xpToNextLevel { get { return CalculateXpCurve(level); } }
    public float currentXp { get { return CalculateCurrentXp(1, _xp); } }


    public float xp
    {
        get
        {
            return _xp;
        }

        set
        {
            if (value <= 0)
            {
                _xp = 0;
            }
            else if (value >= CalculateXpCurve(level))
            {
                _xp = CalculateXpCurve(level);
            }
            else
            {
                _xp = value;
            }
        }
    }


    // Use this for initialization
    void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        Debug.Log(currentXp + " / " + xpToNextLevel + " - " + level);
	}

    int CalculateLevel(int level, float xpleft)
    {
        xpleft = xpleft - CalculateXpCurve(level);
        
        if (xpleft < 0)
        {
            return level;
        }
        else
        {
            return CalculateLevel(++level, xpleft);
        }
    }

    float CalculateCurrentXp(int level, float xpleft)
    {
        xpleft = xpleft - CalculateXpCurve(level);

        if (xpleft < 0)
        {
            return xpleft + CalculateXpCurve(level);
        }
        else
        {
            return CalculateCurrentXp(++level, xpleft);
        }
    }

    float CalculateXpCurve(float x)
    {
        return Mathf.Round(Mathf.Pow(0.4f * x, 2) + (3.5f * x) + 0.27f) * 100;
    }
}
