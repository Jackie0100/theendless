﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public delegate void TouchPan(Vector2 position, Vector2 deltaposition);
public delegate void TouchTap(Vector2 position);
public delegate void TouchHold(Vector2 position, float time);
public delegate void TouchDoubleTap(Vector2 position);
public delegate void TouchPinch(Vector2 position1, Vector2 position2, Vector2 deltaposition1, Vector2 deltaposition2, float distance, float deltadistance);
public delegate void TouchRotate(Vector2 position1, Vector2 position2, Vector2 deltaposition1, Vector2 deltaposition2, float distance);

public class TouchEventListener : MonoBehaviour
{
    public static TouchEventListener Instance { get; private set; }

    public event TouchPan OnTouchPan;
    public event TouchTap OnTouchTap;
    public event TouchDoubleTap OnTouchDoubleTap;
    public event TouchHold OnTouchHold;
    public event TouchPinch OnTouchPinch;
    public event TouchRotate OnTouchRotate;

    [SerializeField]
    private float _holdTimeAmount = 1.0f;
    [SerializeField]
    private float _doubleClickTimer = 0.25f;

#if UNITY_EDITOR || UNITY_WEBGL || UNITY_STANDALONE
    private Vector3 previousMousePosition;
    private Vector2 mouseDelta { get { return Input.mousePosition - previousMousePosition; } } 
#endif
    private bool hasBenOverUI { get; set; }
    private bool hasInvoked { get; set; }
    private bool hasMoved { get; set; }
    private float holdTimer { get; set; }
    private float holdTimeAmount
    {
        get { return _holdTimeAmount; }
        set { _holdTimeAmount = value; }
    }

    public float doubleClickTimer
    {
        get { return _doubleClickTimer; }
        set { _doubleClickTimer = value; }
    }

    // Use this for initialization
    void Awake ()
    {
        Instance = this;
        TouchEventListener.Instance.OnTouchTap += OnTouchTapHandler;
        TouchEventListener.Instance.OnTouchHold += OnTouchHoldHandler;
        TouchEventListener.Instance.OnTouchPan += OnTouchPanHandler;
        TouchEventListener.Instance.OnTouchPinch += OnTouchPinchHandler;
        TouchEventListener.Instance.OnTouchDoubleTap += OnTouchDoubleTapHandler;
        TouchEventListener.Instance.OnTouchRotate += OnTouchRotateHandler;
    }

    // Update is called once per frame
    void Update ()
    {
		if (Input.touchCount > 0)
        {
            if (Input.touchCount == 2)
            {
                if (Input.touches[0].phase == TouchPhase.Moved || Input.touches[1].phase == TouchPhase.Moved)
                {
                    if (OnTouchPinch != null)
                    {
                        OnTouchPinch.Invoke(Input.touches[0].position, Input.touches[1].position, Input.touches[0].deltaPosition, Input.touches[1].deltaPosition,
                            Vector2.Distance(Input.touches[0].position, Input.touches[1].position), Vector2.Distance(Input.touches[0].deltaPosition, Input.touches[1].deltaPosition));
                    }
                    Debug.Log("Pinch Invoke");

                }
            }
            else
            {
                switch (Input.touches[0].phase)
                {
                    case TouchPhase.Began:
                        break;
                    case TouchPhase.Moved:
                        if (hasMoved || Input.touches[0].deltaPosition.magnitude > 5.0f)
                        {
                            hasMoved = true;
                            if (OnTouchPan != null)
                            {
                                OnTouchPan.Invoke(Input.touches[0].position, Input.touches[0].deltaPosition);
                            }
                            Debug.Log("Pan Invoke");
                        }
                        break;
                    case TouchPhase.Stationary:
                        holdTimer += Time.deltaTime;
                        if (holdTimer >= holdTimeAmount && !hasMoved && !hasInvoked)
                        {
                            hasInvoked = true;
                            if (OnTouchHold != null)
                            {
                                OnTouchHold.Invoke(Input.touches[0].position, holdTimer);
                            }
                            Debug.Log("Hold Invoke");
                        }
                        break;
                    case TouchPhase.Ended:
                        if (!hasMoved && !hasInvoked)
                        {
                            if (OnTouchTap != null)
                            {
                                OnTouchTap.Invoke(Input.touches[0].position);
                            }
                            Debug.Log("Tap Invoke");
                        }
                        hasBenOverUI = false;
                        hasMoved = false;
                        hasInvoked = false;
                        holdTimer = 0;
                        break;
                    case TouchPhase.Canceled:
                        hasMoved = false;
                        hasInvoked = false;
                        holdTimer = 0;
                        break;
                }
            }
        }
#if UNITY_EDITOR || UNITY_WEBGL || UNITY_STANDALONE
        else if (Application.isEditor || Application.isWebPlayer && Input.touchCount == 0)
        {
            if (Input.GetKey(KeyCode.Mouse0))
            {
                if (hasMoved || mouseDelta.magnitude > 1.0f)
                {
                    hasMoved = true;
                    OnTouchPanHandler(Input.mousePosition, mouseDelta);
                }
            }
            else if (Input.GetKeyUp(KeyCode.Mouse0) && !hasMoved)
            {
                OnTouchTapHandler(Input.mousePosition);
            }
            else if (Input.GetKeyUp(KeyCode.Mouse1))
            {
                OnTouchHoldHandler(Input.mousePosition, holdTimer);
            }
            else if (Input.GetKey(KeyCode.Mouse2))
            {
                OnTouchRotateHandler(Vector2.zero, Vector2.zero, Vector2.zero, Vector2.zero, 0);
            }
            if (Input.GetAxis("Mouse ScrollWheel") != 0)
            {
                OnTouchPinchHandler(Input.mousePosition, Input.mousePosition, mouseDelta, mouseDelta, Input.GetAxis("Mouse ScrollWheel"), Input.GetAxis("Mouse ScrollWheel"));
            }
        }

        if (Input.GetKeyUp(KeyCode.Mouse0) && hasMoved)
        {
            hasBenOverUI = false;
            hasMoved = false;
        }
        previousMousePosition = Input.mousePosition;
#endif

        if (Input.touchCount == 0 && !(Input.GetKey(KeyCode.Mouse0) || Input.GetKey(KeyCode.Mouse1)))
        {
            hasBenOverUI = false;
        }
    }

    void OnTouchTapHandler(Vector2 position)
    {
        if (!IsinsideUI(position))
        {
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(position), out hit, 100))
            {
                Debug.Log(hit.transform);
                if (hit.transform.GetComponent<IOnTouchTapEvent>() != null)
                {
                    hit.transform.GetComponent<IOnTouchTapEvent>().OnTouchTap(position);
                }
            }
        }
    }

    void OnTouchHoldHandler(Vector2 position, float time)
    {
        if (!IsinsideUI(position))
        {
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(position), out hit, 100))
            {
                Debug.Log(hit.transform);
                if (hit.transform.GetComponent<IOnTouchHoldEvent>() != null)
                {
                    hit.transform.GetComponent<IOnTouchHoldEvent>().OnTouchHold(position, time);
                }
            }
        }
    }

    void OnTouchPanHandler(Vector2 position, Vector2 deltaposition)
    {
        if (!IsinsideUI(position))
        {
            Camera.main.GetComponent<CameraController>().OnTouchPan(position, deltaposition);
        }
    }

    void OnTouchPinchHandler(Vector2 position1, Vector2 position2, Vector2 deltaposition1, Vector2 deltaposition2, float distance, float deltadistance)
    {
        if (!IsinsideUI(position1) && !IsinsideUI(position2))
        {
            Camera.main.GetComponent<CameraController>().OnTouchPinch(position1, position2, deltaposition1, deltaposition2, distance, deltadistance);
        }
    }

    void OnTouchDoubleTapHandler(Vector2 position)
    {

    }

    void OnTouchRotateHandler(Vector2 position1, Vector2 position2, Vector2 deltaposition1, Vector2 deltaposition2, float distance)
    {

    }

    bool IsinsideUI(Vector2 position)
    {
        if (hasBenOverUI)
        {
            return true;
        }
        if (UIController.Instance.CanShow())
        {
            if (UIWindow.currentFocusedWindow != null)
            {
                if (UnityEngine.RectTransformUtility.RectangleContainsScreenPoint(UIWindow.currentFocusedWindow.GetComponent<RectTransform>(), position))
                {
                hasBenOverUI = true;

                return true;
                }
            }
        }
        return false;
    }
}

interface IOnTouchTapEvent
{
    void OnTouchTap(Vector2 position);
}

interface IOnTouchHoldEvent
{
    void OnTouchHold(Vector2 position, float time);
}

interface IOnTouchPanEvent
{
    void OnTouchPan(Vector2 position, Vector2 deltaposition);
}

interface IOnTouchPinchEvent
{
    void OnTouchPinch(Vector2 position1, Vector2 position2, Vector2 deltaposition1, Vector2 deltaposition2, float distance, float deltadistance);
}

interface IOnTouchDoubleTapEvent
{
    void OnTouchDoubleTap(Vector2 position);
}

interface IOnTouchTouchRotateEvent
{
    void OnTouchRotate(Vector2 position1, Vector2 position2, Vector2 deltaposition1, Vector2 deltaposition2, float distance);
}