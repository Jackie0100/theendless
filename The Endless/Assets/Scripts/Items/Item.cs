﻿using System;
using UnityEngine;

public class Items
{
    public string name;
    public string itemText;
    public RarityType rareType;
    public Texture2D icon;
    public int value;
    public int itemLevel;
    public int usableLevel;
    public int stackSize;

    public Items(string _name, string _itemText, int _rareType, Texture2D _icon, int _value, int _itemLevel, int _usableLevel, int _stackSize)
    {
        name = _name;
        itemText = _itemText;
        rareType = (RarityType)_rareType;
        icon = _icon;
        value = _value;
        itemLevel = _itemLevel;
        usableLevel = _usableLevel;
        stackSize = _stackSize;
    }
}
