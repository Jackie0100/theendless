﻿using System;
using UnityEngine;

public class Usable : Items
{
    public int useEffect;
    public int charges;

    public Usable(string _name, string _itemText, int _rareType, Texture2D _icon, int _value, int _itemLevel, int _usableLevel, int _useEffect, int _charges, int _stackSize)
        : base (_name, _itemText, _rareType, _icon, _value, _itemLevel, _usableLevel, _stackSize)
    {
        useEffect = _useEffect;
        charges = _charges;
    }
}
