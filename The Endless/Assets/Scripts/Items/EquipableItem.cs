﻿using System;
using UnityEngine;

public class EquipableItem : Usable
{
    public EquipmentSlot equipmentSlot;
    public int durability;
    public int maxDurability;
    public Stats stats;

    public EquipableItem(string _name, string _itemText, int _rareType, Texture2D _icon, int _value, int _itemLevel, int _usableLevel, int _useEffect, int _charges, int _stackSize, int _equipmentSlot, int _durability, int _maxDurability, Stats _stats)
        : base(_name, _itemText, _rareType, _icon, _value, _itemLevel, _usableLevel, _useEffect, _charges, _stackSize)
    {
        equipmentSlot = (EquipmentSlot)_equipmentSlot;
        durability = _durability;
        maxDurability = _maxDurability;
        stats = _stats;
    }
}
