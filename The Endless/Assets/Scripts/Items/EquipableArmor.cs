﻿using System;
using UnityEngine;

public class Armor : EquipableItem
{
    public ArmorType armorType;
    public int equipEffectID;
    public int chanceWhenHittedEffectID;

    public Armor(string _name, string _itemText, int _rareType, Texture2D _icon, int _value, int _itemLevel, int _usableLevel, int _useEffect, int _charges, int _stackSize, int _equipmentSlot, int _durability, int _maxDurability, Stats _stats, int _armorType, int _equipEffectID, int _chanceWhenHittedEffectID)
        : base(_name, _itemText, _rareType, _icon, _value, _itemLevel, _usableLevel, _useEffect, _charges, _stackSize, _equipmentSlot, _durability, _maxDurability, _stats)
    {
        armorType = (ArmorType)_armorType;
        equipEffectID = _equipEffectID;
        chanceWhenHittedEffectID = _chanceWhenHittedEffectID;
    }
}
