﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Stats
{
    int stamina;
    int intellect;
    int spirit;
    int agility;
    int strength;
    int armor;
    int resistance;
    int parry;
    int dodge;
    int defence;
    int block;
    int attackPower;
    int armorPenetration;
    int meleeHit;
    int meleeCritical;
    int rangedPower;
    int rangedHit;
    int rangedCritical;
    int spellPower;
    int spellCritical;
    int spellHit;
    int spellPenetration;
    int healthRegain;
    int manaRegain;

    public Stats (int stamina, int intellect, int spirit, int agility, int strength, int parry, int dodge, int defence, int block, int attackPower, int armorPenetration, int meleeHit, int meleeCritical, int armor, int resistance, int rangedPower, int rangedHit, int rangedCritical, int spellPower, int spellCritical, int spellHit, int spellPenetration, int healthRegain, int manaRegain)
    {
    }
}
