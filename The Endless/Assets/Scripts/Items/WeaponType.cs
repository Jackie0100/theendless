﻿using System;

public enum WeaponType
{
    Sword, Two_Handed_Sword, Mace, Two_Handed_Mace, Axe, Two_Handed_Axe, Polearm, Spear, Fist_Weapon, Unarmed, Staff, Shield, Wand, Bow, Crossbow, Gun, Throw, Relic, Rune,
}
