﻿using System;
using UnityEngine;

public class Weapon : EquipableItem
{
    public WeaponType weaponType;
    public int minDamage;
    public int maxDamage;
    public float speed;
    public float DPS;
    public int equipEffectID;
    public int chanceonHitID;

    public Weapon(string _name, string _itemText, int _rareType, Texture2D _icon, int _value, int _itemLevel, int _usableLevel, int _useEffect, int _charges, int _stackSize, int _equipmentSlot, int _durability, int _maxDurability, Stats _stats, int _weaponType, int _minDamage, int _maxDamage, float _speed, int _equipEffectID, int _chanceonHitID)
        : base(_name, _itemText, _rareType, _icon, _value, _itemLevel, _usableLevel, _useEffect, _charges, _stackSize, _equipmentSlot, _durability, _maxDurability, _stats)
    {
        weaponType = (WeaponType)_weaponType;
        minDamage = _minDamage;
        maxDamage = _maxDamage;
        speed = _speed;
        equipEffectID = _equipEffectID;
        chanceonHitID = _chanceonHitID;
        DPS = (float)((minDamage + maxDamage) / 2) / speed;
    }
}
