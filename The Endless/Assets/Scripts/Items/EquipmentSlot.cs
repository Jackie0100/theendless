﻿using System;

public enum EquipmentSlot
{
    Head, Neck, Shoulders, Hands, Wrists, Chest, Waist, Legs, Feets, Back, Mainhand, One_Handed, Offhand, Relic, Ring, Trinket,
}
