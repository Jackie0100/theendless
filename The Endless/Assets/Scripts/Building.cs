﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : InteractiveEntity {

    public GameObject UserInterface;

    void Start()
    {
        Instance = this;
    }


    void Update () {
		
	}

    public override void OnTouchTap(Vector2 position)
    {
        Debug.Log("Building Touched");
        ShowUserInterface(UserInterface);
    }

    public void TestUIButton()
    {
        float x = UnityEngine.Random.Range(-80f, 80f);
        float y = 4;
        float z = UnityEngine.Random.Range(-45f, 45f);

        GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube.transform.localScale = new Vector3(8, 8, 8);
        cube.transform.position = new Vector3(x, y, z);
    }
}
