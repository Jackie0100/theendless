﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveEntity : MonoBehaviour, IOnTouchTapEvent {

    public static InteractiveEntity Instance { get; set; }
    void Start()
    {
        Instance = this;

    }

    // Update is called once per frame
    void Update () {

	}

    //this is called by superClasses
    public void ShowUserInterface(GameObject ui)
    {
        UIController.Instance.Show(ui);
    }

    virtual public void OnTouchTap(Vector2 position)
    {
        Debug.Log("IE touched");
        
    }

}
